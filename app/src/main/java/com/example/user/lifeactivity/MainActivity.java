package com.example.user.lifeactivity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    private Integer countOfStudents = 0;

    // Вызывается при создании Активности
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Инициализация Активности
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");
        // Тост
        toast("onCreate");
    }
    // Вызывается, когда Активность стала видимой
    @Override
    protected void onStart() {
        super.onStart();
        // Проделать необходимые действия для
        // Активности, видимой на экране
        Log.d(TAG, "onStart");
        toast("onStart");
    }
    // Должен вызываться в начале видимого состояния.
    // На самом деле Android вызывает данный обработчик только
    // для Активностей, восстановленных из неактивного состояния
    @Override
    protected void onResume() {
        super.onResume();
        // Восстановить приостановленные обновления UI,
        // потоки и процессы, замороженные, когда
        // Активность была в неактивном состоянии
        Log.d(TAG, "onResume");
        toast("onResume");
    }
    // Вызывается перед выходом из активного состояния
    @Override
    protected void onPause() {
        // «Заморозить» обновления UI, потоки или
        // «трудоемкие» процессы, не нужные, когда Активность
        // не на переднем плане
        super.onPause();
        Log.d(TAG, "onPause");
        toast("onPause");
    }
    // Вызывается перед выходом из видимого состояния
    @Override
    protected void onStop() {
        // «Заморозить» обновления UI, потоки или
        // «трудоемкие» процессы, ненужные, когда Активность
        // не на переднем плане.
        // Сохранить все данные и изменения в UI, так как
        // процесс может быть в любой момент убит системой
        super.onStop();
        Log.d(TAG, "onStop");
        toast("onStop");
    }
    // Вызывается перед тем, как Активность снова становится видимой
    @Override
    protected void onRestart() {
        super.onRestart();
        // Восстановление состояние UI из объекта savedInstanceState.
        // Данный объект также был передан методу onCreate.
        Log.d(TAG, "onRestart");
        toast("onRestart");
    }
    // Вызывается перед уничтожением активности
    @Override
    protected void onDestroy() {
        // Освободить все ресурсы, включая работающие потоки,
        // соединения с БД и т. д.
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        toast("onDestroy");
    }
    // Вызывается перед выходом из активного состояния,
    // позволяя сохранить состояние в объекте savedInstanceState
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Объект savedInstanceState будет в последующем
        // передан методам onCreate и onRestoreInstanceState
        super.onSaveInstanceState(outState);
        outState.putInt("count", countOfStudents);
        Log.d(TAG, "onSaveInstanceState");
        toast("onSaveInstanceState");
    }
    // Вызывается после завершения метода onCreate
    // Используется для восстановления состояния UI
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null &&
                savedInstanceState.containsKey("count")) {
            countOfStudents = savedInstanceState.getInt("count");
            outputCountOfStudents();
        }
        Log.d(TAG, "onRestoreInstanceState");
        toast("onRestoreInstanceState");
    }

    public void outputCountOfStudents() {
        TextView counterView = findViewById(R.id.txt_counter);
        counterView.setText(String.format(Locale.getDefault(), "%d", countOfStudents));
    }

    public void onClickButtonAddStudents(View view) {
        countOfStudents++;
        toast("+1");
        outputCountOfStudents();
    }

    public void toast(String state) {
        Toast toast = Toast.makeText(getApplicationContext(),
                state, Toast.LENGTH_SHORT);
        toast.show();
    }
}
